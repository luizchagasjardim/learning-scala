Repeat the preceding assignment, but produce a new array with the swappedvalues. Use for/yield.

I already did this on the preceding question:

def swapAdjacentElements(a : Array[Int]) : Array[Int] = {
    (for (i <- 0 until a.length) yield {
        if (i % 2 == 1) a(i - 1)
        else if (i != a.length - 1) a(i + 1)
        else a(i)
    }).toArray
}
