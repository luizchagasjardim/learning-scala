Rewrite  the  example  at  the  end  of  Section 3.4, “Transforming Arrays,” on page 34 using the drop method for dropping the index of the first match. Look the method up in Scaladoc.

It seems the method remove doesn't exist anymore in Scala, so I thought it was easier to rewrite the whole thing, and then drop wasn't even necessary.

def removeNegativesExceptFirst(a : Array[Int]) = {
    val firstNegativeIndex = a.indexWhere(x => x < 0)
    a.zipWithIndex.filter(t => t._1 >= 0 || t._2 == firstNegativeIndex).map(_._1)
}

scala> removeNegativesExceptFirst(Array(0,1,2,-1,0,2,-5,-1,0))
res0: Array[Int] = Array(0, 1, 2, -1, 0, 2, 0)
