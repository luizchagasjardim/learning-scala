Come up with one situation where the assignment x = y = 1 is valid in Scala. (Hint: Pick a suitable type for x.)

Declare x as Unit or Any. Or simply don't specify x's type:

scala> var y = 0
y: Int = 0

scala> var x = y = 1
x: Unit = ()
